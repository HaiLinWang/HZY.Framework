﻿using HZY.Framework.DependencyInjection.Attributes;

namespace HZY.Framework.DependencyInjection.SourceGenerator.Test;

[Component]
public class AppService<T> //: UserService where T : class
{
    [Autowired]
    public T? User { get; set; }

}
