﻿using HZY.Framework.DependencyInjection.Attributes;

namespace HZY.Framework.DependencyInjection.SourceGenerator.Test;

/// <summary>
/// 
/// </summary>
[Component(ServiceLifetime.Scoped)]
public class UserService : IUserService
{

    [AppSettings("AllowedHosts")]
    private string ConnectionString { get; }

    [Autowired]
    private IConfiguration Configuration { get; }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    //[Time("测试111")]
    public async Task<string> GetName(string name)
    {
        var a = ConnectionString;
        await Task.CompletedTask;

        var aa = Configuration.GetSection("AllowedHosts").Get<string>();

        return "张三";
    }



}
