﻿namespace HZY.Framework.Core.ServerMetricMonitoring.Impl;

/// <summary>
/// 
/// </summary>
public abstract class AbstractServerMetricMonitoringService
{
    /// <summary>
    /// 获取内存指标
    /// </summary>
    /// <returns></returns>
    protected abstract RamInfo GetRamInfo();
}
