﻿namespace HZY.Framework.Core.ServerMetricMonitoring.Impl;

/// <summary>
/// Windows 服务器指标监控服务
/// </summary>
public class WindowsServerMetricMonitoringService : AbstractServerMetricMonitoringService, IServerMetricMonitoringService
{
    /// <summary>
    /// 获取计算机信息
    /// </summary>
    /// <returns></returns>
    public virtual RamInfo GetComputerInfo()
    {
        var ramInfo = this.GetRamInfo();
        ramInfo.CpuRate = Math.Ceiling(this.GetCpuRate().ToDouble()) + "%";
        return ramInfo;
    }

    /// <summary>
    /// 获取CPU使用率
    /// </summary>
    /// <returns></returns>
    public virtual string GetCpuRate()
    {
        string output = OperatingSystemUtil.Cmd("wmic", "cpu get LoadPercentage");
        return output.Replace("LoadPercentage", string.Empty).Trim();
    }

    /// <summary>
    /// 获取服务器运行时间
    /// </summary>
    /// <returns></returns>
    public virtual string GetRunTime()
    {
        var runTime = string.Empty;
        string output = OperatingSystemUtil.Cmd("wmic", "OS get LastBootUpTime/Value");
        string[] outputArr = output.Split('=', (char)StringSplitOptions.RemoveEmptyEntries);
        if (outputArr.Length == 2)
        {
            runTime = (DateTime.Now - outputArr[1].Split('.')[0].ToDateTimeByString()).ToDayHourMinutesSeconds();
        }
        return runTime;
    }

    /// <summary>
    /// 获取磁盘信息
    /// </summary>
    /// <returns></returns>
    public virtual List<DiskInfo> GetDiskInfos()
    {
        var result = new List<DiskInfo>();

        var driveInfos = DriveInfo.GetDrives();

        foreach (var item in driveInfos)
        {
            try
            {
                var diskInfo = new DiskInfo()
                {
                    DiskName = item.Name,
                    TypeName = item.DriveType.ToString(),
                    TotalSize = item.TotalSize / 1024 / 1024 / 1024,
                    AvailableFreeSpace = item.AvailableFreeSpace / 1024 / 1024 / 1024,
                };
                diskInfo.Used = diskInfo.TotalSize - diskInfo.AvailableFreeSpace;
                diskInfo.AvailablePercent = decimal.Ceiling(diskInfo.Used / (decimal)diskInfo.TotalSize * 100);

                result.Add(diskInfo);
            }
            catch
            {
                continue;
            }
        }

        return result;
    }

    /// <summary>
    /// 获取内存指标
    /// </summary>
    /// <returns></returns>
    protected override RamInfo GetRamInfo()
    {
        string output = OperatingSystemUtil.Cmd("wmic", "OS get FreePhysicalMemory,TotalVisibleMemorySize /Value");
        var ramInfo = new RamInfo();
        var lines = output.Trim().Split('\n', (char)StringSplitOptions.RemoveEmptyEntries);

        if (lines.Length <= 0) return ramInfo;

        var freeMemoryParts = lines[0].Split('=', (char)StringSplitOptions.RemoveEmptyEntries);
        var totalMemoryParts = lines[1].Split('=', (char)StringSplitOptions.RemoveEmptyEntries);

        ramInfo.Total = Math.Round(double.Parse(totalMemoryParts[1]) / 1024, 0);
        ramInfo.Free = Math.Round(double.Parse(freeMemoryParts[1]) / 1024, 0);
        ramInfo.Used = ramInfo.Total - ramInfo.Free;

        return ramInfo;
    }
}
