﻿namespace HZY.Framework.Core.ServerMetricMonitoring.Models;

/// <summary>
/// 磁盘信息
/// </summary>
public class DiskInfo
{
    /// <summary>
    /// 磁盘名
    /// </summary>
    public string? DiskName { get; set; }

    /// <summary>
    /// 磁盘类型
    /// </summary>
    public string? TypeName { get; set; }

    /// <summary>
    /// 空闲总数
    /// </summary>
    public long TotalFree { get; set; }

    /// <summary>
    /// 磁盘大小
    /// </summary>
    public long TotalSize { get; set; }

    /// <summary>
    /// 已使用
    /// </summary>
    public long Used { get; set; }

    /// <summary>
    /// 可使用
    /// </summary>
    public long AvailableFreeSpace { get; set; }

    /// <summary>
    /// 可用百分比
    /// </summary>
    public decimal AvailablePercent { get; set; }


}
