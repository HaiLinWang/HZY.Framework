﻿namespace HZY.Framework.Core.ServerMetricMonitoring.Models;

/// <summary>
/// 内存指标信息
/// </summary>
public class RamInfo
{
    /// <summary>
    /// 总字节数
    /// </summary>
    public double Total { get; set; }

    /// <summary>
    /// 已使用字节数
    /// </summary>
    public double Used { get; set; }

    /// <summary>
    /// 空闲字节数
    /// </summary>
    public double Free { get; set; }

    /// <summary>
    /// CPU使用率 %
    /// </summary>
    public string? CpuRate { get; set; }

    /// <summary>
    /// 已使用内存
    /// </summary>
    public string? RamUsed { get { return Math.Round(this.Used / 1024, 2) + "GB"; } }

    /// <summary>
    /// 总内存 GB
    /// </summary>
    public string? RamTotal { get { return Math.Round(this.Total / 1024, 2) + "GB"; } }

    /// <summary>
    /// 内存使用率 %
    /// </summary>
    public string? RamRate { get { return Math.Ceiling(100 * this.Used / this.Total).ToString() + "%"; } }

    /// <summary>
    /// 空闲内存
    /// </summary>
    public string? RamFree { get { return Math.Round(this.Free / 1024, 2) + "GB"; } }

}
