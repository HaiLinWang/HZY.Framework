﻿/*
 * *******************************************************
 *
 * 作者：HZY
 *
 * 开源地址：https://gitee.com/hzy6
 *
 * *******************************************************
 */

namespace HZY.Framework.Core.Utils;

/// <summary>
/// 读取 xml 摘要 工具
/// </summary>
public static class ReadXmlSummaryUtil
{
    private static Dictionary<Assembly, XmlDocument> XmlCache { get; set; } = new();

    /// <summary>
    /// 通过 MemberInfo 获取属性或者 字段的 xml 注释信息
    /// </summary>
    /// <param name="memberInfo"></param>
    /// <returns></returns>
    public static XmlElement XmlForMember(MemberInfo memberInfo)
    {
        var xmlElement = XmlFromName(memberInfo.DeclaringType, memberInfo.MemberType.ToString()[0],
            memberInfo.Name);
        return xmlElement;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <param name="prefix"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    private static XmlElement XmlFromName(Type type, char prefix, string name)
    {
        string fullName;

        if (string.IsNullOrEmpty(name))
        {
            fullName = prefix + ":" + type.FullName;
        }
        else
        {
            fullName = prefix + ":" + type.FullName + "." + name;
        }

        var xmlDocument = XmlFromAssembly(type.Assembly);

        XmlElement matchedElement = null;

        if (xmlDocument == null) return default;

        foreach (XmlElement xmlElement in xmlDocument["doc"]["members"])
        {
            if (xmlElement.Attributes["name"].Value.Equals(fullName))
            {
                matchedElement = xmlElement;
            }
        }

        return matchedElement;
    }

    private static XmlDocument XmlFromAssembly(Assembly assembly)
    {
        if (!XmlCache.ContainsKey(assembly))
        {
            // load the docuemnt into the cache
            XmlCache[assembly] = XmlFromAssemblyNonCached(assembly);
        }

        return XmlCache[assembly];
    }

    private static XmlDocument XmlFromAssemblyNonCached(Assembly assembly)
    {
        string location = assembly.Location;

        StreamReader streamReader;

        streamReader = new StreamReader(Path.ChangeExtension(location, ".xml"));

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.Load(streamReader);
        return xmlDocument;
    }
}