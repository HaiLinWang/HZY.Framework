﻿/*
 * *******************************************************
 *
 * 作者：HZY
 *
 * 开源地址：https://gitee.com/hzy6
 *
 * *******************************************************
 */

namespace HZY.Framework.Core.Utils;

/// <summary>
/// 框架工具
/// </summary>
public static class StringUtil
{
    /// <summary>
    /// string
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string? ToStr<T>(this T value)
        => value == null ? string.Empty : value.ToString();

    /// <summary>
    /// 首字母小写写
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string ToFirstCharConvertLower(this string input)
    {
        if (string.IsNullOrWhiteSpace(input))
            return input;

        //if (input.Length == input.Count(w => char.IsUpper(w)))
        //{
        //    return input.ToLower();
        //}

        return input.First().ToString().ToLower() + input.Substring(1);
    }

    /// <summary>
    /// 首字母大写
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string ToFirstCharConvertUpper(this string input)
    {
        if (string.IsNullOrWhiteSpace(input))
            return input;
        return input.First().ToString().ToUpper() + input.Substring(1);
    }


    /// <summary>
    /// 蛇形命名 转化为 驼峰命名
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public static string ToLineConvertHump(this string name)
    {
        if (!name.Contains('_') && name != name.ToLower())
        {
            return name;
        }

        StringBuilder sb = new StringBuilder();
        foreach (var item in name.Split("_", StringSplitOptions.RemoveEmptyEntries))
        {
            sb.Append(Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(item));
        }

        return sb.ToString();
    }

    /// <summary>
    /// 将驼峰命名法改为 下划线 命名法
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string ToHumpConvertLine(this string input)
    {
        if (string.IsNullOrWhiteSpace(input))
        {
            return input;
        }

        return Regex.Replace(input, @"([a-z0-9])([A-Z])", "$1_$2").ToLower();
    }

    /// <summary>
    /// 中横线转驼峰命名
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public static string ToLineMiddleConvertHump(this string name)
    {
        if (!name.Contains('-') && name != name.ToLower())
        {
            return name;
        }

        var sb = new StringBuilder();
        foreach (var item in name.Split("-", StringSplitOptions.RemoveEmptyEntries))
        {
            sb.Append(Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(item));
        }

        return sb.ToString();
    }

    /// <summary>
    /// 驼峰转中横线命名
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string ToHumpConvertLineMiddle(this string input)
    {
        if (string.IsNullOrWhiteSpace(input))
        {
            return input;
        }

        return Regex.Replace(input, @"([a-z0-9])([A-Z])", "$1-$2").ToLower();
    }


}
