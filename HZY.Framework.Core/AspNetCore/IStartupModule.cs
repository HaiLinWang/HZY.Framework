﻿namespace HZY.Framework.Core.AspNetCore;

/// <summary>
/// 启动模块
/// </summary>
public interface IStartupModule
{
    /// <summary>
    /// 启动器执行循序，从小到大的顺序执行
    /// </summary>
    int Order { get; set; }

    /// <summary>
    /// 配置服务
    /// </summary>
    /// <param name="webApplicationBuilder"></param>
    void ConfigureServices(WebApplicationBuilder webApplicationBuilder);

    /// <summary>
    /// 配置
    /// </summary>
    /// <param name="webApplication"></param>
    void Configure(WebApplication webApplication);

    /// <summary>
    /// 程序启动 可以获取程序启动 urls
    /// </summary>
    /// <param name="webApplication"></param>
    void ApplicationStarted(WebApplication webApplication);

    /// <summary>
    /// 程序停止中
    /// </summary>
    /// <param name="webApplication"></param>
    void ApplicationStopping(WebApplication webApplication);

    /// <summary>
    /// 程序已经停止
    /// </summary>
    /// <param name="webApplication"></param>
    void ApplicationStopped(WebApplication webApplication);

    /// <summary>
    /// 获取导入的所有启动器实例
    /// </summary>
    /// <returns></returns>
    List<IStartupModule> GetAllImportStartup();
}
