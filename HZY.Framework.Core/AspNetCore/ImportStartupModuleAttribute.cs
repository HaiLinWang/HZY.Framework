﻿namespace HZY.Framework.Core.AspNetCore;

/// <summary>
/// 导入启动模块
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
public class ImportStartupModuleAttribute : Attribute
{
    private readonly List<Type> _startupTypes = new();
    private readonly List<IStartupModule> _startups = new();

    /// <summary>
    /// 导入启动模块
    /// </summary>
    /// <param name="startupTypes"></param>
    public ImportStartupModuleAttribute(params Type[] startupTypes)
    {
        this.Init(startupTypes);
    }

    /// <summary>
    /// 初始化
    /// </summary>
    protected void Init(params Type[] startupTypes)
    {
        _startupTypes.AddRange(startupTypes);

        foreach (var item in _startupTypes)
        {
            var startup = Activator.CreateInstance(item) as IStartupModule;

            if (startup == null) continue;

            _startups.Add(startup);
        }
    }

    /// <summary>
    /// 获取启动器集合
    /// </summary>
    /// <returns></returns>
    public List<Type> GetStartupTypes() => _startupTypes;

    /// <summary>
    /// 获取启动器实例集合
    /// </summary>
    /// <returns></returns>
    public List<IStartupModule> GetStartups() => _startups;


}

#if NET8_0_OR_GREATER

/// <summary>
/// 导入启动模块
/// </summary>
/// <typeparam name="TStartupModule1"></typeparam>
public class ImportStartupModuleAttribute<TStartupModule1> : ImportStartupModuleAttribute
    where TStartupModule1 : IStartupModule
{
    /// <summary>
    /// 导入启动器
    /// </summary>
    public ImportStartupModuleAttribute() : base(typeof(TStartupModule1))
    {

    }
}

/// <summary>
/// 导入启动模块
/// </summary>
/// <typeparam name="TStartupModule1"></typeparam>
/// <typeparam name="TStartupModule2"></typeparam>
public class ImportStartupModuleAttribute<TStartupModule1, TStartupModule2> : ImportStartupModuleAttribute
    where TStartupModule1 : IStartupModule
    where TStartupModule2 : IStartupModule
{
    /// <summary>
    /// 导入启动器
    /// </summary>
    public ImportStartupModuleAttribute() : base(
        typeof(TStartupModule1),
        typeof(TStartupModule2))
    {

    }
}

/// <summary>
/// 导入启动模块
/// </summary>
/// <typeparam name="TStartupModule1"></typeparam>
/// <typeparam name="TStartupModule2"></typeparam>
/// <typeparam name="TStartupModule3"></typeparam>
public class ImportStartupModuleAttribute<TStartupModule1,
    TStartupModule2,
    TStartupModule3> : ImportStartupModuleAttribute
    where TStartupModule1 : IStartupModule
    where TStartupModule2 : IStartupModule
    where TStartupModule3 : IStartupModule
{
    /// <summary>
    /// 导入启动器
    /// </summary>
    public ImportStartupModuleAttribute() : base(
        typeof(TStartupModule1),
        typeof(TStartupModule2),
        typeof(TStartupModule3))
    {

    }
}

/// <summary>
/// 导入启动模块
/// </summary>
/// <typeparam name="TStartupModule1"></typeparam>
/// <typeparam name="TStartupModule2"></typeparam>
/// <typeparam name="TStartupModule3"></typeparam>
/// <typeparam name="TStartupModule4"></typeparam>
public class ImportStartupModuleAttribute<TStartupModule1,
    TStartupModule2,
    TStartupModule3,
    TStartupModule4> : ImportStartupModuleAttribute
    where TStartupModule1 : IStartupModule
    where TStartupModule2 : IStartupModule
    where TStartupModule3 : IStartupModule
    where TStartupModule4 : IStartupModule
{
    /// <summary>
    /// 导入启动器
    /// </summary>
    public ImportStartupModuleAttribute() : base(
        typeof(TStartupModule1),
        typeof(TStartupModule2),
        typeof(TStartupModule3),
        typeof(TStartupModule4))
    {

    }
}

/// <summary>
/// 导入启动模块
/// </summary>
/// <typeparam name="TStartupModule1"></typeparam>
/// <typeparam name="TStartupModule2"></typeparam>
/// <typeparam name="TStartupModule3"></typeparam>
/// <typeparam name="TStartupModule4"></typeparam>
/// <typeparam name="TStartupModule5"></typeparam>
public class ImportStartupModuleAttribute<TStartupModule1,
    TStartupModule2,
    TStartupModule3,
    TStartupModule4,
    TStartupModule5> : ImportStartupModuleAttribute
    where TStartupModule1 : IStartupModule
    where TStartupModule2 : IStartupModule
    where TStartupModule3 : IStartupModule
    where TStartupModule4 : IStartupModule
    where TStartupModule5 : IStartupModule
{
    /// <summary>
    /// 导入启动器
    /// </summary>
    public ImportStartupModuleAttribute() : base(
        typeof(TStartupModule1),
        typeof(TStartupModule2),
        typeof(TStartupModule3),
        typeof(TStartupModule4),
        typeof(TStartupModule5))
    {

    }
}

/// <summary>
/// 导入启动模块
/// </summary>
/// <typeparam name="TStartupModule1"></typeparam>
/// <typeparam name="TStartupModule2"></typeparam>
/// <typeparam name="TStartupModule3"></typeparam>
/// <typeparam name="TStartupModule4"></typeparam>
/// <typeparam name="TStartupModule5"></typeparam>
/// <typeparam name="TStartupModule6"></typeparam>
public class ImportStartupModuleAttribute<TStartupModule1,
    TStartupModule2,
    TStartupModule3,
    TStartupModule4,
    TStartupModule5,
    TStartupModule6> : ImportStartupModuleAttribute
    where TStartupModule1 : IStartupModule
    where TStartupModule2 : IStartupModule
    where TStartupModule3 : IStartupModule
    where TStartupModule4 : IStartupModule
    where TStartupModule5 : IStartupModule
    where TStartupModule6 : IStartupModule
{
    /// <summary>
    /// 导入启动器
    /// </summary>
    public ImportStartupModuleAttribute() : base(
        typeof(TStartupModule1),
        typeof(TStartupModule2),
        typeof(TStartupModule3),
        typeof(TStartupModule4),
        typeof(TStartupModule5),
        typeof(TStartupModule6))
    {

    }
}

/// <summary>
/// 导入启动模块
/// </summary>
/// <typeparam name="TStartupModule1"></typeparam>
/// <typeparam name="TStartupModule2"></typeparam>
/// <typeparam name="TStartupModule3"></typeparam>
/// <typeparam name="TStartupModule4"></typeparam>
/// <typeparam name="TStartupModule5"></typeparam>
/// <typeparam name="TStartupModule6"></typeparam>
/// <typeparam name="TStartupModule7"></typeparam>
public class ImportStartupModuleAttribute<TStartupModule1,
    TStartupModule2,
    TStartupModule3,
    TStartupModule4,
    TStartupModule5,
    TStartupModule6,
    TStartupModule7> : ImportStartupModuleAttribute
    where TStartupModule1 : IStartupModule
    where TStartupModule2 : IStartupModule
    where TStartupModule3 : IStartupModule
    where TStartupModule4 : IStartupModule
    where TStartupModule5 : IStartupModule
    where TStartupModule6 : IStartupModule
    where TStartupModule7 : IStartupModule
{
    /// <summary>
    /// 导入启动器
    /// </summary>
    public ImportStartupModuleAttribute() : base(
        typeof(TStartupModule1),
        typeof(TStartupModule2),
        typeof(TStartupModule3),
        typeof(TStartupModule4),
        typeof(TStartupModule5),
        typeof(TStartupModule6),
        typeof(TStartupModule7))
    {

    }
}

/// <summary>
/// 导入启动模块
/// </summary>
/// <typeparam name="TStartupModule1"></typeparam>
/// <typeparam name="TStartupModule2"></typeparam>
/// <typeparam name="TStartupModule3"></typeparam>
/// <typeparam name="TStartupModule4"></typeparam>
/// <typeparam name="TStartupModule5"></typeparam>
/// <typeparam name="TStartupModule6"></typeparam>
/// <typeparam name="TStartupModule7"></typeparam>
/// <typeparam name="TStartupModule8"></typeparam>
public class ImportStartupModuleAttribute<TStartupModule1,
    TStartupModule2,
    TStartupModule3,
    TStartupModule4,
    TStartupModule5,
    TStartupModule6,
    TStartupModule7,
    TStartupModule8> : ImportStartupModuleAttribute
    where TStartupModule1 : IStartupModule
    where TStartupModule2 : IStartupModule
    where TStartupModule3 : IStartupModule
    where TStartupModule4 : IStartupModule
    where TStartupModule5 : IStartupModule
    where TStartupModule6 : IStartupModule
    where TStartupModule7 : IStartupModule
    where TStartupModule8 : IStartupModule
{
    /// <summary>
    /// 导入启动器
    /// </summary>
    public ImportStartupModuleAttribute() : base(
        typeof(TStartupModule1),
        typeof(TStartupModule2),
        typeof(TStartupModule3),
        typeof(TStartupModule4),
        typeof(TStartupModule5),
        typeof(TStartupModule6),
        typeof(TStartupModule7),
        typeof(TStartupModule8))
    {

    }
}

/// <summary>
/// 导入启动模块
/// </summary>
/// <typeparam name="TStartupModule1"></typeparam>
/// <typeparam name="TStartupModule2"></typeparam>
/// <typeparam name="TStartupModule3"></typeparam>
/// <typeparam name="TStartupModule4"></typeparam>
/// <typeparam name="TStartupModule5"></typeparam>
/// <typeparam name="TStartupModule6"></typeparam>
/// <typeparam name="TStartupModule7"></typeparam>
/// <typeparam name="TStartupModule8"></typeparam>
/// <typeparam name="TStartupModule9"></typeparam>
public class ImportStartupModuleAttribute<TStartupModule1,
    TStartupModule2,
    TStartupModule3,
    TStartupModule4,
    TStartupModule5,
    TStartupModule6,
    TStartupModule7,
    TStartupModule8,
    TStartupModule9> : ImportStartupModuleAttribute
    where TStartupModule1 : IStartupModule
    where TStartupModule2 : IStartupModule
    where TStartupModule3 : IStartupModule
    where TStartupModule4 : IStartupModule
    where TStartupModule5 : IStartupModule
    where TStartupModule6 : IStartupModule
    where TStartupModule7 : IStartupModule
    where TStartupModule8 : IStartupModule
    where TStartupModule9 : IStartupModule
{
    /// <summary>
    /// 导入启动器
    /// </summary>
    public ImportStartupModuleAttribute() : base(
        typeof(TStartupModule1),
        typeof(TStartupModule2),
        typeof(TStartupModule3),
        typeof(TStartupModule4),
        typeof(TStartupModule5),
        typeof(TStartupModule6),
        typeof(TStartupModule7),
        typeof(TStartupModule8),
        typeof(TStartupModule9))
    {

    }
}

/// <summary>
/// 导入启动模块
/// </summary>
/// <typeparam name="TStartupModule1"></typeparam>
/// <typeparam name="TStartupModule2"></typeparam>
/// <typeparam name="TStartupModule3"></typeparam>
/// <typeparam name="TStartupModule4"></typeparam>
/// <typeparam name="TStartupModule5"></typeparam>
/// <typeparam name="TStartupModule6"></typeparam>
/// <typeparam name="TStartupModule7"></typeparam>
/// <typeparam name="TStartupModule8"></typeparam>
/// <typeparam name="TStartupModule9"></typeparam>
/// <typeparam name="TStartupModule10"></typeparam>
public class ImportStartupModuleAttribute<TStartupModule1,
    TStartupModule2,
    TStartupModule3,
    TStartupModule4,
    TStartupModule5,
    TStartupModule6,
    TStartupModule7,
    TStartupModule8,
    TStartupModule9,
    TStartupModule10> : ImportStartupModuleAttribute
    where TStartupModule1 : IStartupModule
    where TStartupModule2 : IStartupModule
    where TStartupModule3 : IStartupModule
    where TStartupModule4 : IStartupModule
    where TStartupModule5 : IStartupModule
    where TStartupModule6 : IStartupModule
    where TStartupModule7 : IStartupModule
    where TStartupModule8 : IStartupModule
    where TStartupModule9 : IStartupModule
    where TStartupModule10 : IStartupModule
{
    /// <summary>
    /// 导入启动器
    /// </summary>
    public ImportStartupModuleAttribute() : base(
        typeof(TStartupModule1),
        typeof(TStartupModule2),
        typeof(TStartupModule3),
        typeof(TStartupModule4),
        typeof(TStartupModule5),
        typeof(TStartupModule6),
        typeof(TStartupModule7),
        typeof(TStartupModule8),
        typeof(TStartupModule9),
        typeof(TStartupModule10))
    {

    }
}

/// <summary>
/// 导入启动模块
/// </summary>
/// <typeparam name="TStartupModule1"></typeparam>
/// <typeparam name="TStartupModule2"></typeparam>
/// <typeparam name="TStartupModule3"></typeparam>
/// <typeparam name="TStartupModule4"></typeparam>
/// <typeparam name="TStartupModule5"></typeparam>
/// <typeparam name="TStartupModule6"></typeparam>
/// <typeparam name="TStartupModule7"></typeparam>
/// <typeparam name="TStartupModule8"></typeparam>
/// <typeparam name="TStartupModule9"></typeparam>
/// <typeparam name="TStartupModule10"></typeparam>
/// <typeparam name="TStartupModule11"></typeparam>
public class ImportStartupModuleAttribute<TStartupModule1,
    TStartupModule2,
    TStartupModule3,
    TStartupModule4,
    TStartupModule5,
    TStartupModule6,
    TStartupModule7,
    TStartupModule8,
    TStartupModule9,
    TStartupModule10,
    TStartupModule11> : ImportStartupModuleAttribute
    where TStartupModule1 : IStartupModule
    where TStartupModule2 : IStartupModule
    where TStartupModule3 : IStartupModule
    where TStartupModule4 : IStartupModule
    where TStartupModule5 : IStartupModule
    where TStartupModule6 : IStartupModule
    where TStartupModule7 : IStartupModule
    where TStartupModule8 : IStartupModule
    where TStartupModule9 : IStartupModule
    where TStartupModule10 : IStartupModule
    where TStartupModule11 : IStartupModule
{
    /// <summary>
    /// 导入启动器
    /// </summary>
    public ImportStartupModuleAttribute() : base(
        typeof(TStartupModule1),
        typeof(TStartupModule2),
        typeof(TStartupModule3),
        typeof(TStartupModule4),
        typeof(TStartupModule5),
        typeof(TStartupModule6),
        typeof(TStartupModule7),
        typeof(TStartupModule8),
        typeof(TStartupModule9),
        typeof(TStartupModule10),
        typeof(TStartupModule11))
    {

    }
}

/// <summary>
/// 导入启动模块
/// </summary>
/// <typeparam name="TStartupModule1"></typeparam>
/// <typeparam name="TStartupModule2"></typeparam>
/// <typeparam name="TStartupModule3"></typeparam>
/// <typeparam name="TStartupModule4"></typeparam>
/// <typeparam name="TStartupModule5"></typeparam>
/// <typeparam name="TStartupModule6"></typeparam>
/// <typeparam name="TStartupModule7"></typeparam>
/// <typeparam name="TStartupModule8"></typeparam>
/// <typeparam name="TStartupModule9"></typeparam>
/// <typeparam name="TStartupModule10"></typeparam>
/// <typeparam name="TStartupModule11"></typeparam>
/// <typeparam name="TStartupModule12"></typeparam>
public class ImportStartupModuleAttribute<TStartupModule1,
    TStartupModule2,
    TStartupModule3,
    TStartupModule4,
    TStartupModule5,
    TStartupModule6,
    TStartupModule7,
    TStartupModule8,
    TStartupModule9,
    TStartupModule10,
    TStartupModule11,
    TStartupModule12> : ImportStartupModuleAttribute
    where TStartupModule1 : IStartupModule
    where TStartupModule2 : IStartupModule
    where TStartupModule3 : IStartupModule
    where TStartupModule4 : IStartupModule
    where TStartupModule5 : IStartupModule
    where TStartupModule6 : IStartupModule
    where TStartupModule7 : IStartupModule
    where TStartupModule8 : IStartupModule
    where TStartupModule9 : IStartupModule
    where TStartupModule10 : IStartupModule
    where TStartupModule11 : IStartupModule
    where TStartupModule12 : IStartupModule
{
    /// <summary>
    /// 导入启动器
    /// </summary>
    public ImportStartupModuleAttribute() : base(
        typeof(TStartupModule1),
        typeof(TStartupModule2),
        typeof(TStartupModule3),
        typeof(TStartupModule4),
        typeof(TStartupModule5),
        typeof(TStartupModule6),
        typeof(TStartupModule7),
        typeof(TStartupModule8),
        typeof(TStartupModule9),
        typeof(TStartupModule10),
        typeof(TStartupModule11),
        typeof(TStartupModule12))
    {

    }
}

#endif