﻿namespace HZY.Framework.Core.Models;

/// <summary>
/// app 配置
/// </summary>
public class AppOptions
{
    /// <summary>
    /// 通过程序集扫描定时任务开关
    /// </summary>
    public bool AssemblyScanScheduledSwitch { get; set; } = false;

    /// <summary>
    /// 
    /// </summary>
    public List<Assembly>? Assemblies { get; set; } = new();

    /// <summary>
    /// WebApplicationBuilder 构建器，可以配置服务
    /// </summary>
    public Action<WebApplicationBuilder>? WebApplicationBuilderAction { get; set; }

    /// <summary>
    /// WebApplicationAction
    /// </summary>
    public Action<WebApplication>? WebApplicationAction { get; set; }
}