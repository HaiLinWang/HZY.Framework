﻿namespace HZY.Framework.FormulaParser.Linq;

/// <summary>
/// 可拦截 执行函数
/// </summary>
/// <remarks>
/// 接口，以允许具有不同功能的invoke方法的不同实现。
/// 确保向后兼容性和行为。
/// </remarks>
public interface IInvokeMethod
{

    /// <summary>
    /// 在给定传递给方法的一组参数/参数的情况下，调用对象（targetobject）内的方法
    /// </summary>
    /// <returns>对方法返回值的对象引用</returns>
    object? Invoke(object targetObject, string methodName, object[] args);
    
    /// <summary>
    /// 在给定传递给方法的一组参数/参数的情况下，拦截委托执行函数
    /// </summary>
    /// <param name="delegate">委托</param>
    /// <param name="args"></param>
    /// <returns>对方法返回值的对象引用</returns>
    object? InvokeDelegate(Delegate @delegate, object[] args);

}
