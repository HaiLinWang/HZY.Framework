﻿namespace HZY.Framework.FormulaParser.Linq;

/// <summary>
/// 表示由生成的表达式中的值<see cref="LambdaParser"/>.
/// </summary>
public interface ILambdaValue
{
    object Value { get; }
}
