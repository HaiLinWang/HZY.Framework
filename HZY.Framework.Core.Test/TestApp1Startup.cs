﻿using HZY.Framework.Core.AspNetCore;

namespace HZY.Framework.Core.Test;

[ImportStartupModule(typeof(TestApp2Startup), typeof(TestApp2Startup))]
public class TestApp1Startup : StartupModule<TestApp1Startup>
{

    public override void ConfigureServices(WebApplicationBuilder webApplicationBuilder)
    {
        Console.WriteLine($"{nameof(TestApp1Startup)} = {nameof(ConfigureServices)}");
    }

    public override void Configure(WebApplication webApplication)
    {
        Console.WriteLine($"{nameof(TestApp1Startup)} = {nameof(Configure)}");
    }


}
