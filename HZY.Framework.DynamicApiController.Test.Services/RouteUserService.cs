﻿using Microsoft.AspNetCore.Mvc;

namespace HZY.Framework.DynamicApiController.Test.Services
{
    //[ApiExplorerSettings(IgnoreApi = true)]
    public class RouteUserService : AppServiceControllerBase
    {
        [HttpGet]
        public string OkAsync()
        {
            return "123";
        }

        [NonAction]
        public string BadAsync()
        {
            return "111";
        }


    }
}