﻿global using HZY.Framework.DynamicApiController.Core;
global using Microsoft.AspNetCore.Mvc;
global using Microsoft.AspNetCore.Mvc.ActionConstraints;
global using Microsoft.AspNetCore.Mvc.ApplicationModels;
global using Microsoft.AspNetCore.Mvc.Controllers;
global using Microsoft.AspNetCore.Mvc.Routing;
global using Microsoft.Extensions.DependencyInjection;
global using System.Reflection;
global using System.Text;
global using System.Text.RegularExpressions;