﻿namespace HZY.Framework.DynamicApiController.Core;

/// <summary>
/// api 参数项
/// </summary>
public class DynamicApiControllerOptions
{
    /// <summary>
    /// 控制器 和 action 路由用小写
    /// </summary>
    public bool IsLower { get; set; } = true;

    /// <summary>
    /// api 接口 识别后缀集合
    /// </summary>
    public string[] ControllerTypeNameSuffixs { get; } = Enum.GetNames<DynamicApiControllerTypeNameSuffixEnum>();

    /// <summary>
    /// HttpMethod 映射关系
    /// </summary>
    public Dictionary<string, DynamicApiControllerHttpMethodEnum> HttpMethodRule { get; } = new()
    {
        ["post"] = DynamicApiControllerHttpMethodEnum.POST,
        ["add"] = DynamicApiControllerHttpMethodEnum.POST,
        ["create"] = DynamicApiControllerHttpMethodEnum.POST,
        ["insert"] = DynamicApiControllerHttpMethodEnum.POST,
        ["submit"] = DynamicApiControllerHttpMethodEnum.POST,
        ["get"] = DynamicApiControllerHttpMethodEnum.GET,
        ["find"] = DynamicApiControllerHttpMethodEnum.GET,
        ["fetch"] = DynamicApiControllerHttpMethodEnum.GET,
        ["query"] = DynamicApiControllerHttpMethodEnum.GET,
        ["put"] = DynamicApiControllerHttpMethodEnum.PUT,
        ["update"] = DynamicApiControllerHttpMethodEnum.PUT,
        ["delete"] = DynamicApiControllerHttpMethodEnum.DELETE,
        ["remove"] = DynamicApiControllerHttpMethodEnum.DELETE,
        ["clear"] = DynamicApiControllerHttpMethodEnum.DELETE,
        ["patch"] = DynamicApiControllerHttpMethodEnum.PATCH,
        ["options"] = DynamicApiControllerHttpMethodEnum.OPTIONS,

        //["post"] = "POST",
        //["add"] = "POST",
        //["create"] = "POST",
        //["insert"] = "POST",
        //["submit"] = "POST",
        //["get"] = "GET",
        //["find"] = "GET",
        //["fetch"] = "GET",
        //["query"] = "GET",
        //["put"] = "PUT",
        //["update"] = "PUT",
        //["delete"] = "DELETE",
        //["remove"] = "DELETE",
        //["clear"] = "DELETE",
        //["patch"] = "PATCH"

    };


}
