using HZY.Framework.Repository.EntityFramework;
using HZY.Framework.Repository.EntityFramework.Test.DbContexts;
using HZY.Framework.Repository.EntityFramework.Test.Models;
using HZY.Framework.Repository.EntityFramework.Test.Repositories;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers().AddControllersAsServices();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//ȡ������֤
builder.Host.UseDefaultServiceProvider(options => { options.ValidateScopes = false; });


#region ע��

// ע��������
builder.Services.AddDbContextPool<AppDbContext>((serviceProvider, options) =>
   {
       //
       //options.AddEntityFrameworkRepositories();

       options.UseSqlServer(@"Server=.;Database=hzy_admin_sqlserver_20230227;User ID=sa;Password=123456;MultipleActiveResultSets=true;Encrypt=True;TrustServerCertificate=True;");
       options.UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole()));

   });
// ע��ִ�
builder.Services.AddEntityFrameworkRepositories(typeof(AppDbContext1));

#endregion

//builder.Services.AddScoped<TestService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//EntityFrameworkRepositoriesExtensions.UseEntityFrameworkRepositories(typeof(AppDbContext1));

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
