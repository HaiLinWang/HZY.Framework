﻿using HZY.Framework.Repository.EntityFramework.Repositories.Impl;
using HZY.Framework.Repository.EntityFramework.Test.DbContexts;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace HZY.Framework.Repository.EntityFramework.Test.Repositories
{
    public class AppRepository1<T> : RepositoryBaseImpl<T, DbContext> where T : class, new()
    {
        public AppRepository1(Expression<Func<T, bool>> filter = null) : base(null, filter)
        {

        }

    }

    public class AppRepository<T> : RepositoryBaseImpl<T, AppDbContext> where T : class, new()
    {
        public AppRepository(AppDbContext appDbContext, Expression<Func<T, bool>> filter = null) : base(appDbContext, filter)
        {

        }
    }
}
