﻿using HZY.Framework.Repository.EntityFramework.Attributes;
using HZY.Framework.Repository.EntityFramework.Repositories;
using HZY.Framework.Repository.EntityFramework.Repositories.Impl;
using HZY.Framework.Repository.EntityFramework.Test.Models;
using Microsoft.EntityFrameworkCore;

namespace HZY.Framework.Repository.EntityFramework.Test.DbContexts
{
    //[DbContextConfig("HZY.Framework.Repository.EntityFramework.Test.Models.*")]
    public class AppDbContext : DbContext, IBaseDbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            UnitOfWork = new UnitOfWorkImpl<AppDbContext>(this);
        }

        public IUnitOfWork UnitOfWork { get; }

        DbSet<SysFunction> SysFunction { get; set; }

    }

    [DbContextConfig("HZY.Framework.Repository.EntityFramework.Test.Models.*")]
    public class AppDbContext1 : DbContext
    {
        public AppDbContext1(DbContextOptions<AppDbContext1> options) : base(options)
        {

        }

        DbSet<SysFunction> SysFunction { get; set; }

    }


}
