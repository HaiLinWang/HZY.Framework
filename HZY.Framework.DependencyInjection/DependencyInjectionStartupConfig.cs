﻿/*
 * *******************************************************
 *
 * 作者：hzy
 *
 * 开源地址：https://gitee.com/hzy6
 *
 * *******************************************************
 */


namespace HZY.Framework.DependencyInjection;

public static class DependencyInjectionStartupConfig
{
    /// <summary>
    /// 自动注册服务
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="assemblyList"></param>
    /// <param name="closeScanComponentAttribute">关闭扫描 ComponentAttribute 特性</param>
    public static void AddDependencyInjection(this IServiceCollection serviceCollection, List<Assembly>? assemblyList, bool closeScanComponentAttribute = false)
    {
        if (assemblyList == null) return;

        DynamicProxyClass.Start(serviceCollection, assemblyList, closeScanComponentAttribute);
    }

}