using HZY.Framework.Core.Aop.Attributes;

namespace HZY.Framework.DependencyInjection.Attributes;

/// <summary>
/// 属性注入实例
/// </summary>
[AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
public class AutowiredAttribute : AopMoAttribute
{
    public override void OnEntry(MethodContext context)
    {
        //if (context.ReturnValue != null) return;

        if (context.RealReturnType == null) return;

        var result = GetService(context.RealReturnType);

        if (result == null) return;

        context.ReplaceReturnValue(this, result);
    }

    public override void OnException(MethodContext context)
    {

    }

    public override void OnSuccess(MethodContext context)
    {

    }

    public override void OnExit(MethodContext context)
    {
        base.OnExit(context);
    }

}