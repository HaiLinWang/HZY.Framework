﻿namespace HZY.Framework.DependencyInjection;

/// <summary>
/// 注册 Scoped
/// </summary>
public interface IScopedDependency { }

/// <summary>
/// 注册 Scoped 自身实现类
/// </summary>
public interface IScopedSelfDependency { }

/// <summary>
///  注册 Singleton
/// </summary>
public interface ISingletonDependency { }

/// <summary>
///  注册 Singleton 自身实现类
/// </summary>
public interface ISingletonSelfDependency { }

/// <summary>
///  注册 Transient
/// </summary>
public interface ITransientDependency { }

/// <summary>
///  注册 Transient 自身实现类
/// </summary>
public interface ITransientSelfDependency { }