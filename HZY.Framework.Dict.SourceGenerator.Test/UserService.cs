﻿// using Rougamo;
// using Rougamo.Context;
//
// namespace HZY.Framework.Dict.SourceGenerator.Test;
//
// // 在继承MoAttribute的同时，重写Flags属性，未重写时默认InstancePublic(可访问性为public的实例方法或属性)
// public class LoggingAttribute : MoAttribute
// {
//     public override void OnEntry(MethodContext context)
//     {
//         // 从context对象中能取到包括入参、类实例、方法描述等信息
//         Console.WriteLine("方法执行前");
//     }
//
//     public override void OnException(MethodContext context)
//     {
//         Console.WriteLine("方法执行异常");
//     }
//
//     public override void OnSuccess(MethodContext context)
//     {
//         Console.WriteLine("方法执行成功后");
//     }
//
//     public override void OnExit(MethodContext context)
//     {
//         Console.WriteLine("方法退出时，不论方法执行成功还是异常，都会执行");
//     }
// }
//
// [Logging]
// public class UserService
// {
//     [Logging]
//     public string GetName() => "123";
// }