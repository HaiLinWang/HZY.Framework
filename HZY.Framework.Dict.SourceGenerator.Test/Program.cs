﻿// See https://aka.ms/new-console-template for more information

using HZY.Api.Demo;
using HZY.Framework.Dict.SourceGenerator.Test;

var aopClassService = new AopClassService();

var name = aopClassService.GetName();
//
// var userService = new UserService();
// var name2 = userService.GetName();

var userInfo = new UserInfo();
userInfo.NameDictText = "666";

Console.WriteLine("Hello, World! " + nameof(UserInfo.NameDictText) + " = " + userInfo.NameDictText);

Console.ReadLine();