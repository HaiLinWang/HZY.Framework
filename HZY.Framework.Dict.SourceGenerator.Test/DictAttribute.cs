﻿namespace HZY.Framework.Dict.SourceGenerator.Test
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DictAttribute : Attribute
    {

        public string TableName { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }


    }


}

