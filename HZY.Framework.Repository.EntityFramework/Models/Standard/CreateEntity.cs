﻿namespace HZY.Framework.Repository.EntityFramework.Models.Standard;

/// <summary>
/// 创建
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class CreateEntity<TKey> : Entity<TKey>, ICreateEntity
{
    /// <summary>
    /// 创建人 id
    /// </summary>
    public virtual Guid? CreatorUserId { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public virtual DateTime CreationTime { get; set; } = DateTime.Now;
}

/// <summary>
/// 创建
/// </summary>
public interface ICreateEntity : IEntity
{
    /// <summary>
    /// 创建用户
    /// </summary>
    Guid? CreatorUserId { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    DateTime CreationTime { get; set; }
}