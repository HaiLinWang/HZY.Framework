﻿namespace HZY.Framework.Repository.EntityFramework.Models.Standard;

/// <summary>
/// 更新
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class UpdateEntity<TKey> : CreateEntity<TKey>, IUpdateEntity
{
    /// <summary>
    /// 最后更新人
    /// </summary>
    public virtual Guid? LastModifierUserId { get; set; }

    /// <summary>
    /// 最后更新时间
    /// </summary>
    public virtual DateTime? LastModificationTime { get; set; }
}

/// <summary>
/// 更新
/// </summary>
public interface IUpdateEntity : IEntity
{
    /// <summary>
    /// 更新用户
    /// </summary>
    Guid? LastModifierUserId { get; set; }

    /// <summary>
    /// 更新时间
    /// </summary>
    DateTime? LastModificationTime { get; set; }
}