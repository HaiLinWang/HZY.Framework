﻿namespace HZY.Framework.Repository.EntityFramework.Models.StandardV2;

/// <summary>
/// 更新
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class UpdateEntityV2<TKey> : CreateEntityV2<TKey>, IUpdateEntityV2
{
    /// <summary>
    /// 最后更新人id
    /// </summary>
    public virtual string? UpdateBy { get; set; }

    /// <summary>
    /// 最后更新时间
    /// </summary>
    public virtual DateTime? UpdateTime { get; set; }
}

/// <summary>
/// 更新
/// </summary>
public interface IUpdateEntityV2 : IEntity
{
    /// <summary>
    /// 更新用户
    /// </summary>
    string? UpdateBy { get; set; }

    /// <summary>
    /// 更新时间
    /// </summary>
    DateTime? UpdateTime { get; set; }
}