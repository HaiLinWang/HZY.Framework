﻿namespace HZY.Framework.Repository.EntityFramework.Models.StandardV2;

/// <summary>
/// 创建
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class CreateEntityV2<TKey> : Entity<TKey>, ICreateEntityV2
{
    /// <summary>
    /// 创建人 id
    /// </summary>
    public virtual string? CreateBy { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public virtual DateTime CreateTime { get; set; } = DateTime.Now;
}

/// <summary>
/// 创建
/// </summary>
public interface ICreateEntityV2 : IEntity
{
    /// <summary>
    /// 创建用户
    /// </summary>
    string? CreateBy { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    DateTime CreateTime { get; set; }
}