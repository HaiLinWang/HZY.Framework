﻿namespace HZY.Framework.Repository.EntityFramework.Models.StandardV2;

/// <summary>
/// 基础模型 主键为 Guid
/// 包含属性 Id=主键、
/// UpdateTime=最后更新时间 、
/// UpdateBy=最后更新人Id、
/// CreateTime=创建时间 、
/// CreateBy=创建人Id
/// </summary>
public class DefaultEntityV2 : UpdateEntityV2<Guid>
{

}

/// <summary>
/// 基础模型 主键为 int 自增
/// 包含属性 Id=主键、
/// UpdateTime=最后更新时间 、
/// UpdateBy=最后更新人Id、
/// CreateTime=创建时间 、
/// CreateBy=创建人Id
/// </summary>
public class DefaultEntityIdentityIntKeyV2 : UpdateEntityV2<int>
{
    /// <summary>
    /// 主键
    /// </summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public override int Id { get; set; }
}


/// <summary>
/// 基础模型 主键为 int 自增
/// 包含属性 Id=主键、
/// UpdateTime=最后更新时间 、
/// UpdateBy=最后更新人Id、
/// CreateTime=创建时间 、
/// CreateBy=创建人Id
/// </summary>
public class DefaultEntityIdentityLongKeyV2 : UpdateEntityV2<long>
{
    /// <summary>
    /// 主键
    /// </summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public override long Id { get; set; }
}

/// <summary>
/// 基础模型 主键为 string
/// 包含属性 Id=主键、
/// UpdateTime=最后更新时间 、
/// UpdateBy=最后更新人Id、
/// CreateTime=创建时间 、
/// CreateBy=创建人Id
/// </summary>
public class DefaultEntityStringKeyV2 : UpdateEntityV2<string>
{

}

/// <summary>
/// 基础模型 主键为 自己定义传入
/// 包含属性 Id=主键、
/// UpdateTime=最后更新时间 、
/// UpdateBy=最后更新人Id、
/// CreateTime=创建时间 、
/// CreateBy=创建人Id
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class DefaultEntityV2<TKey> : UpdateEntityV2<TKey>
{

}