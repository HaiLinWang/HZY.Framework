﻿namespace HZY.Framework.Repository.EntityFramework.Models.StandardV2;

/// <summary>
/// 全量模型 主键为 Guid
/// 包含属性 
/// Id=主键、
/// UpdateTime=最后更新时间 、
/// UpdateBy=最后更新人Id、
/// CreateTime=创建时间 、
/// CreateBy=创建人Id、
/// DeletedFlag=是否删除、
/// DeleteBy=删除人、
/// DeleteTime=删除时间
/// </summary>
public class FullEntityV2 : DeleteEntityV2<Guid>
{
}

/// <summary>
/// 全量模型 主键为 int 自增
/// 包含属性 
/// Id=主键、
/// UpdateTime=最后更新时间 、
/// UpdateBy=最后更新人Id、
/// CreateTime=创建时间 、
/// CreateBy=创建人Id、
/// DeletedFlag=是否删除、
/// DeleteBy=删除人、
/// DeleteTime=删除时间
/// </summary>
public class FullEntityIdentityIntKeyV2 : DeleteEntityV2<int>
{
    /// <summary>
    /// 主键
    /// </summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public override int Id { get; set; }
}

/// <summary>
/// 全量模型 主键为 int 自增
/// 包含属性 
/// Id=主键、
/// UpdateTime=最后更新时间 、
/// UpdateBy=最后更新人Id、
/// CreateTime=创建时间 、
/// CreateBy=创建人Id、
/// DeletedFlag=是否删除、
/// DeleteBy=删除人、
/// DeleteTime=删除时间
/// </summary>
public class FullEntityIdentityLongKeyV2 : DeleteEntityV2<long>
{
    /// <summary>
    /// 主键
    /// </summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public override long Id { get; set; }
}

/// <summary>
/// 基础模型 主键为 string
/// 包含属性 
/// Id=主键、
/// UpdateTime=最后更新时间 、
/// UpdateBy=最后更新人Id、
/// CreateTime=创建时间 、
/// CreateBy=创建人Id、
/// DeletedFlag=是否删除、
/// DeleteBy=删除人、
/// DeleteTime=删除时间
/// </summary>
public class FullEntityStringKeyV2 : DeleteEntityV2<string>
{
}

/// <summary>
/// 全量模型 主键为 自己定义传入
/// 包含属性 
/// Id=主键、
/// UpdateTime=最后更新时间 、
/// UpdateBy=最后更新人Id、
/// CreateTime=创建时间 、
/// CreateBy=创建人Id、
/// DeletedFlag=是否删除、
/// DeleteBy=删除人、
/// DeleteTime=删除时间
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class FullEntityV2<TKey> : DeleteEntityV2<TKey>
{
}