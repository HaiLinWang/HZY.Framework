﻿namespace HZY.Framework.Repository.EntityFramework.Models;

/// <summary>
/// 唯一 id
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class Entity<TKey> : IEntity<TKey>
{
    /// <summary>
    /// 唯一 id 
    /// </summary>
    [Key]
    public virtual TKey Id { get; set; }
}

/// <summary>
/// 唯一 id 带有自增特性
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class EntityIdentity<TKey> : Entity<TKey>
{
    /// <summary>
    /// 唯一 id 
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Key]
    public override TKey Id { get; set; }
}

/// <summary>
/// 唯一 id 带有自增特性
/// </summary>
public class EntityIdentityLong : EntityIdentity<long>
{

}

/// <summary>
/// 唯一 id 带有自增特性
/// </summary>
public class EntityIdentityInt : EntityIdentity<int>
{

}

/// <summary>
/// 
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IEntity<TKey> : IEntity
{
    /// <summary>
    /// 唯一 id 
    /// </summary>
    TKey Id { get; set; }
}

/// <summary>
/// dbset 自动填充标识
/// </summary>
public interface IEntity
{

}