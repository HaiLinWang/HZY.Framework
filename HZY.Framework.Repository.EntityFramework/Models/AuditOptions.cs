﻿namespace HZY.Framework.Repository.EntityFramework.Models;

/// <summary>
/// 审计信息
/// </summary>
public class AuditOptions
{
    /// <summary>
    /// 创建时间字段名称
    /// </summary>
    public string? CreationTimeFieldName { get; set; }

    /// <summary>
    /// 创建人Id字段名称
    /// </summary>
    public string? CreatorUserIdFieldName { get; set; }

    /// <summary>
    /// 最后更新时间字段名称
    /// </summary>
    public string? LastModificationTimeFieldName { get; set; }

    /// <summary>
    /// 最后更新人Id字段名称
    /// </summary>
    public string? LastModifierUserIdFieldName { get; set; }

    /// <summary>
    /// 删除人Id字段名称
    /// </summary>
    public string? DeleterUserIdFieldName { get; set; }

    /// <summary>
    /// 删除时间字段名称
    /// </summary>
    public string? DeletionTimeFieldName { get; set; }

    /// <summary>
    /// 是否删除字段名称
    /// </summary>
    public string? IsDeletedFieldName { get; set; }


}
