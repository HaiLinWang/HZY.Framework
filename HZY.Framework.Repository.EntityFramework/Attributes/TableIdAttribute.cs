﻿namespace HZY.Framework.Repository.EntityFramework.Attributes;

/// <summary>
/// 表 id
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
public class TableIdAttribute : Attribute
{
    /// <summary>
    /// id 类型
    /// </summary>
    public IdType IdType { get; set; } = IdType.SnowflakeId;

    /// <summary>
    /// 表 id
    /// </summary>
    public TableIdAttribute()
    {
    }

    /// <summary>
    /// 表 id
    /// </summary>
    /// <param name="idType"></param>
    public TableIdAttribute(IdType idType)
    {
        IdType = idType;
    }
}

/// <summary>
/// id 类型
/// </summary>
public enum IdType
{
    /// <summary>
    /// 雪花id
    /// </summary>
    SnowflakeId = 0,

    /// <summary>
    /// uuid
    /// </summary>
    UuId = 1,

    /// <summary>
    /// uuid 字符串
    /// </summary>
    UuIdString = 2
}